# AWS Target Group Module Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-lb-target-group.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-lb-target-group/)

Terraform module which creates Target Group resources on AWS. Might be used for different modules

These types of resources are supported:

* [Target Group](https://www.terraform.io/docs/providers/aws/r/lb_target_group.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "tg_lb" {
  source = "./module-aws-lb-target-group"

  name = "training-prod"

  resource_identities = {
    tg = "web"
  }

  global_tags         = "${var.global_tags}"
  create_target_group = "true"
  vpc_id              = "${module.vpc.vpc_id}"
}

variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"` 
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    tg   = "web"
  }
```

Both variables should be declared in module section: 
```hcl
module "tg_lb" {
  ......
  name = "training-prod"
  resource_identities = {
    tg   = "web"
  }
  ......
}  
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["tg"])}"
```


## Resource Tagging

There are three level of resource tagging declaration
* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "kms" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "kms_tags" {
  description = "Custome KMS Tags"
  default     = {
    role = "encryption"
  }
}
```

Combination of all this variables should cover all the tagging requirements. 
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| create\_target\_group | Set to true to create target Groups | string | `true` | no |
| deregistration\_delay | The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. The range is 0-3600 seconds. | string | `300` | no |
| global\_tags | Global Tags | map | - | yes |
| health\_check\_healthy\_threshold | The number of consecutive health checks successes required before considering an unhealthy target healthy | string | `3` | no |
| health\_check\_interval | The approximate amount of time, in seconds, between health checks of an individual target. Minimum value 5 seconds, Maximum value 300 seconds. | string | `30` | no |
| health\_check\_matcher | The number of consecutive health check failures required before considering the target unhealthy. For Network Load Balancers, this value must be the same as the healthy_threshold. | string | `200,202` | no |
| health\_check\_path | (Required for HTTP/HTTPS ALB) The destination for the health check request. Applies to Application Load Balancers only (HTTP/HTTPS), not Network Load Balancers (TCP). | string | `/index.html` | no |
| health\_check\_port | The port to use to connect with the target. Valid values are either ports 1-65536, or traffic-port | string | `traffic-port` | no |
| health\_check\_protocol | The protocol to use to connect with the target | string | `HTTP` | no |
| health\_check\_timeout | The amount of time, in seconds, during which no response means a failed health check | string | `5` | no |
| health\_check\_unhealthy\_threshold | The number of consecutive health check failures required before considering the target unhealthy. For Network Load Balancers, this value must be the same as the `healthy_threshold` | string | `3` | no |
| load\_balancer\_type | The type of load balancer to create. Possible values are application or network. | string | `application` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| port | The port on which targets receive traffic, unless overridden when registering a specific target. | string | `80` | no |
| protocol | The protocol to use for routing traffic to the targets. Should be one of `TCP`, `HTTP`, `HTTPS` or `TLS`. | string | `HTTP` | no |
| proxy\_protocol\_v2 | Boolean to enable / disable support for proxy protocol v2 on Network Load Balancers. | string | `false` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| slow\_start | he amount time for targets to warm up before the load balancer sends them a full share of requests. | string | `0` | no |
| stickiness\_cookie\_duration | The time period, in seconds, during which requests from a client should be routed to the same target. After this time period expires, the load balancer-generated cookie is considered stale. The range is 1 second to 1 week (604800 seconds) | string | `86400` | no |
| stickiness\_enabled | Boolean to enable / disable stickiness. | string | `true` | no |
| stickiness\_type | The type of sticky sessions. The only current possible value is lb_cookie. | string | `lb_cookie` | no |
| target\_group\_count | Number of Target Groups to create | string | `1` | no |
| target\_type | The type of target that you must specify when registering targets with this target group. The possible values are instance (targets are specified by instance ID) or ip (targets are specified by IP address) | string | `instance` | no |
| vpc\_id | VPC ID to place Target Groups into | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| default\_target\_group\_arn | Default Target Group ARN - First in the list |
| default\_target\_group\_arn\_suffix | Default Target Group ARN Sufix - First in the list - Used for CloudWatch |
| default\_target\_group\_id | Default Target Group ID - First in the list |
| default\_target\_group\_name | Default Target Group Name - First in the list |
| target\_group\_arn\_suffixes | Full List of Target Groups ARN Suffixes |
| target\_group\_arns | Full List of Target Groups |
| target\_group\_ids | Full List of Target Groups IDs |
| target\_group\_names | Full List of Target Groups Names |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


