locals {
  proxy_protocol_v2    = "${var.load_balancer_type == "network" ? var.proxy_protocol_v2 : "" }"
  health_check_path    = "${var.load_balancer_type == "application" ? var.health_check_path : "" }"
  health_check_matcher = "${var.load_balancer_type == "application" ? var.health_check_matcher : "" }"
}

resource "aws_lb_target_group" "application" {
  count = "${var.create_target_group && var.load_balancer_type == "application" && var.target_group_count > 0 ? var.target_group_count : 0}"

  name                 = "${format("%s-%s-%d", var.name, var.resource_identities["tg"], count.index + 1 )}"
  port                 = "${var.port}"
  protocol             = "${var.protocol}"
  vpc_id               = "${var.vpc_id}"
  deregistration_delay = "${var.deregistration_delay}"
  slow_start           = "${var.slow_start}"
  proxy_protocol_v2    = "${local.proxy_protocol_v2}"
  target_type          = "${var.target_type}"

  stickiness {
    type            = "${var.stickiness_type}"
    cookie_duration = "${var.stickiness_cookie_duration}"
    enabled         = "${var.stickiness_enabled}"
  }

  health_check {
    interval            = "${var.health_check_interval}"
    path                = "${local.health_check_path}"
    port                = "${var.health_check_port}"
    protocol            = "${var.health_check_protocol}"
    timeout             = "${var.health_check_timeout}"
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    matcher             = "${local.health_check_matcher}"
  }
}

resource "aws_lb_target_group" "network" {
  count = "${var.create_target_group && var.load_balancer_type == "network" && var.target_group_count > 0 ? var.target_group_count : 0}"

  name                 = "${format("%s-%s-%d", var.name, var.resource_identities["tg"], count.index + 1 )}"
  port                 = "${var.port}"
  protocol             = "${var.protocol}"
  vpc_id               = "${var.vpc_id}"
  deregistration_delay = "${var.deregistration_delay}"
  slow_start           = "${var.slow_start}"
  proxy_protocol_v2    = "${local.proxy_protocol_v2}"
  target_type          = "${var.target_type}"

  health_check {
    port     = "${var.health_check_port}"
    protocol = "${var.health_check_protocol}"
  }
}
