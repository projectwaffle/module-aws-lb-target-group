output "default_target_group_id" {
  description = "Default Target Group ID - First in the list"
  value       = "${element(concat(aws_lb_target_group.application.*.id, aws_lb_target_group.network.*.id, list("")), 0)}"
}

output "target_group_ids" {
  description = "Full List of Target Groups IDs"
  value       = "${concat(aws_lb_target_group.application.*.id, aws_lb_target_group.network.*.id, list(""))}"
}

output "default_target_group_arn" {
  description = "Default Target Group ARN - First in the list"
  value       = "${element(concat(aws_lb_target_group.application.*.arn, aws_lb_target_group.network.*.arn, list("")), 0)}"
}

output "target_group_arns" {
  description = "Full List of Target Groups"
  value       = "${concat(aws_lb_target_group.application.*.arn, aws_lb_target_group.network.*.arn, list(""))}"
}

output "default_target_group_arn_suffix" {
  description = "Default Target Group ARN Sufix - First in the list - Used for CloudWatch"
  value       = "${element(concat(aws_lb_target_group.application.*.arn_suffix, aws_lb_target_group.network.*.arn_suffix, list("")), 0)}"
}

output "target_group_arn_suffixes" {
  description = "Full List of Target Groups ARN Suffixes"
  value       = "${concat(aws_lb_target_group.application.*.arn_suffix, aws_lb_target_group.network.*.arn_suffix, list(""))}"
}

output "default_target_group_name" {
  description = "Default Target Group Name - First in the list"
  value       = "${element(concat(aws_lb_target_group.application.*.name, aws_lb_target_group.network.*.name, list("")), 0)}"
}

output "target_group_names" {
  description = "Full List of Target Groups Names"
  value       = "${concat(aws_lb_target_group.application.*.name, aws_lb_target_group.network.*.name, list(""))}"
}
