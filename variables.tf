## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

variable "vpc_id" {
  description = "VPC ID to place Target Groups into"
}

variable "load_balancer_type" {
  description = " The type of load balancer to create. Possible values are application or network."
  default     = "application"
}

variable "create_target_group" {
  description = "Set to true to create target Groups"
  default     = "true"
}

variable "target_group_count" {
  description = "Number of Target Groups to create"
  default     = "1"
}

variable "port" {
  description = "The port on which targets receive traffic, unless overridden when registering a specific target."
  default     = "80"
}

variable "protocol" {
  description = "The protocol to use for routing traffic to the targets. Should be one of `TCP`, `HTTP`, `HTTPS` or `TLS`."
  default     = "HTTP"
}

variable "deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. The range is 0-3600 seconds."
  default     = "300"
}

variable "slow_start" {
  description = "he amount time for targets to warm up before the load balancer sends them a full share of requests. "
  default     = "0"
}

variable "proxy_protocol_v2" {
  description = "Boolean to enable / disable support for proxy protocol v2 on Network Load Balancers. "
  default     = "false"
}

variable "stickiness_type" {
  description = "The type of sticky sessions. The only current possible value is lb_cookie."
  default     = "lb_cookie"
}

variable "stickiness_cookie_duration" {
  description = "The time period, in seconds, during which requests from a client should be routed to the same target. After this time period expires, the load balancer-generated cookie is considered stale. The range is 1 second to 1 week (604800 seconds)"
  default     = "86400"
}

variable "stickiness_enabled" {
  description = "Boolean to enable / disable stickiness. "
  default     = "true"
}

variable "health_check_interval" {
  description = "The approximate amount of time, in seconds, between health checks of an individual target. Minimum value 5 seconds, Maximum value 300 seconds."
  default     = "30"
}

variable "health_check_path" {
  description = "(Required for HTTP/HTTPS ALB) The destination for the health check request. Applies to Application Load Balancers only (HTTP/HTTPS), not Network Load Balancers (TCP)."
  default     = "/index.html"
}

variable "health_check_port" {
  description = "The port to use to connect with the target. Valid values are either ports 1-65536, or traffic-port"
  default     = "traffic-port"
}

variable "health_check_protocol" {
  description = "The protocol to use to connect with the target"
  default     = "HTTP"
}

variable "health_check_timeout" {
  description = "The amount of time, in seconds, during which no response means a failed health check"
  default     = "5"
}

variable "health_check_healthy_threshold" {
  description = "The number of consecutive health checks successes required before considering an unhealthy target healthy"
  default     = "3"
}

variable "health_check_unhealthy_threshold" {
  description = "The number of consecutive health check failures required before considering the target unhealthy. For Network Load Balancers, this value must be the same as the `healthy_threshold`"
  default     = "3"
}

variable "health_check_matcher" {
  description = "The number of consecutive health check failures required before considering the target unhealthy. For Network Load Balancers, this value must be the same as the healthy_threshold. "
  default     = "200,202"
}

variable "target_type" {
  description = "The type of target that you must specify when registering targets with this target group. The possible values are instance (targets are specified by instance ID) or ip (targets are specified by IP address)"
  default     = "instance"
}
